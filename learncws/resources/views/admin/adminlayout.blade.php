<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Code with Sadiq @yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>
<nav class="nav-extended black">
    <div class="nav-wrapper">
     <div class="container">
     <a href="#" class="brand-logo">Admin Panel</a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="sass.html" class="btn red">Logout</a></li>
      </ul>
     </div>
    </div>
    <div class="nav-content">
      <ul class="tabs tabs-transparent">
        <li class="tab"><a class="active" href="#courses">Courses</a></li>
        <li class="tab"><a href="#students">Students</a></li>
        <li class="tab disabled"><a href="#chapters">Chapters</a></li>
        <li class="tab"><a href="#quiz">Quiz</a></li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">Sass</a></li>
    <li><a href="badges.html">Components</a></li>
    <li><a href="collapsible.html">JavaScript</a></li>
  </ul>

  <div id="courses" class="col s12">
      @section('courses')
        @show()
  </div>
  <div id="students" class="col s12">Test 2</div>
  <div id="chapters" class="col s12">Test 3</div>
  <div id="quiz" class="col s12">Test 4</div>
       
    
     
      @section('content')
        @show()
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="{{ URL::asset('js/main.js')}}"></script>
</body>
</html>